# EmlaLock API

This is the official EmlaLock API for JavaScript and TypeScript.

## Installation

```bash
npm install @emlalock/api
```

## Usage

```typescript
import { EmlaLock, Info } from '@emlalock/api';

const emlalockApi: EmlaLock = new EmlaLock();

const info: Info | ErrorObject = await emlalockApi.getInfo({userid: "abc123", apikey: "secret"});
```

## License

MIT
