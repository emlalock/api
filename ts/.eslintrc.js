module.exports = {
    env: {
        browser: true,
        node: true
    },
    extends: ["plugin:@typescript-eslint/recommended", "plugin:@typescript-eslint/recommended-requiring-type-checking"],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        project: "tsconfig.lint.json",
        sourceType: "module"
    },
    plugins: [
        "eslint-plugin-import",
        "eslint-plugin-unicorn",
        "eslint-plugin-no-null",
        "eslint-plugin-prefer-arrow",
        "@typescript-eslint",
        "@typescript-eslint/tslint",
        "unused-imports"
    ],
    rules: {
        // regeln erstmal deaktiviert
        "@typescript-eslint/no-unsafe-assignment": "off",
        "@typescript-eslint/no-unsafe-member-access": "off",
        "@typescript-eslint/no-unsafe-return": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-unsafe-call": "off",
        "@typescript-eslint/no-misused-promises": "off",
        "@typescript-eslint/no-dynamic-delete": "off",
        "class-methods-use-this": "off",
        "@typescript-eslint/strict-boolean-expressions": "off",
        "@typescript-eslint/prefer-regexp-exec": "off",
        "@typescript-eslint/no-extraneous-class": "off",
        "@typescript-eslint/no-unsafe-argument": "off",
        //
        "unused-imports/no-unused-imports": "error",
        "unused-imports/no-unused-vars": [
            "warn",
            {
                vars: "all",
                varsIgnorePattern: "^_",
                args: "after-used",
                argsIgnorePattern: "^_"
            }
        ],
        "@typescript-eslint/no-unused-vars": "off",
        "@typescript-eslint/adjacent-overload-signatures": "error",
        "@typescript-eslint/array-type": [
            "error",
            {
                default: "array-simple"
            }
        ],
        "@typescript-eslint/await-thenable": "error",
        "@typescript-eslint/ban-types": "off",
        "@typescript-eslint/consistent-type-assertions": "error",
        "@typescript-eslint/consistent-type-definitions": "error",
        "@typescript-eslint/dot-notation": "error",
        "@typescript-eslint/explicit-member-accessibility": [
            "error",
            {
                accessibility: "explicit",
                overrides: {
                    constructors: "no-public"
                }
            }
        ],
        "@typescript-eslint/indent": [
            "error",
            4,
            {
                FunctionDeclaration: {
                    parameters: "first"
                },
                FunctionExpression: {
                    parameters: "first"
                },
                SwitchCase: 1
            }
        ],
        "@typescript-eslint/member-delimiter-style": [
            "error",
            {
                multiline: {
                    delimiter: "semi",
                    requireLast: true
                },
                singleline: {
                    delimiter: "semi",
                    requireLast: false
                }
            }
        ],
        "@typescript-eslint/member-ordering": [
            "error",
            {
                default: [
                    "public-static-field",
                    "protected-static-field",
                    "private-static-field",

                    "public-instance-field",
                    "protected-instance-field",
                    "private-instance-field",

                    "public-abstract-field",
                    "protected-abstract-field",
                    "private-abstract-field",

                    "public-field",
                    "protected-field",
                    "private-field",

                    "static-field",
                    "instance-field",
                    "abstract-field",

                    "field",

                    "signature",

                    "constructor",

                    "public-static-method",
                    "protected-static-method",
                    "private-static-method",

                    "public-instance-method",
                    "protected-instance-method",
                    "private-instance-method",

                    "public-abstract-method",
                    "protected-abstract-method",
                    "private-abstract-method",

                    "public-method",
                    "protected-method",
                    "private-method",

                    "static-method",
                    "instance-method",
                    "abstract-method",

                    "method"
                ]
            }
        ],
        "@typescript-eslint/naming-convention": [
            "error",
            {
                selector: "interface",
                format: ["PascalCase"]
            }
        ],
        "@typescript-eslint/no-empty-function": "off",
        "@typescript-eslint/no-empty-interface": "error",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-floating-promises": "error",
        "@typescript-eslint/no-for-in-array": "error",
        "@typescript-eslint/no-inferrable-types": "off",
        "@typescript-eslint/no-misused-new": "error",
        "@typescript-eslint/no-namespace": "error",
        "@typescript-eslint/no-non-null-assertion": "error",
        "@typescript-eslint/no-parameter-properties": "off",
        "@typescript-eslint/no-require-imports": "error",
        "@typescript-eslint/no-shadow": [
            "error",
            {
                hoist: "all"
            }
        ],
        "@typescript-eslint/no-this-alias": "error",
        "@typescript-eslint/no-unnecessary-boolean-literal-compare": "error",
        "@typescript-eslint/no-unnecessary-qualifier": "error",
        "@typescript-eslint/no-unnecessary-type-arguments": "error",
        "@typescript-eslint/no-unnecessary-type-assertion": "off",
        "@typescript-eslint/no-unused-expressions": [
            "error",
            {
                allowShortCircuit: true
            }
        ],
        "@typescript-eslint/no-use-before-define": "off",
        "@typescript-eslint/no-var-requires": "error",
        "@typescript-eslint/prefer-for-of": "off",
        "@typescript-eslint/prefer-function-type": "error",
        "@typescript-eslint/prefer-namespace-keyword": "error",
        "@typescript-eslint/prefer-readonly": "error",
        "@typescript-eslint/promise-function-async": "error",
        "@typescript-eslint/quotes": ["error", "double", { avoidEscape: true }],
        "@typescript-eslint/restrict-plus-operands": "error",
        "@typescript-eslint/semi": ["error", "always"],
        "@typescript-eslint/triple-slash-reference": [
            "error",
            {
                path: "always",
                types: "prefer-import",
                lib: "always"
            }
        ],
        "@typescript-eslint/type-annotation-spacing": "error",
        "@typescript-eslint/unbound-method": "off",
        "@typescript-eslint/unified-signatures": "error",
        "arrow-body-style": ["error", "as-needed"],
        "arrow-parens": ["error", "as-needed"],
        "brace-style": ["error", "1tbs"],
        "comma-dangle": "error",
        "complexity": [
            "error",
            {
                max: 50
            }
        ],
        "constructor-super": "error",
        "curly": ["error", "multi-line"],
        "default-case": "off",
        "dot-notation": "off",
        "eol-last": "error",
        "eqeqeq": ["error", "smart"],
        "guard-for-in": "error",
        "id-blacklist": ["error", "any", "Number", "number", "String", "string", "Boolean", "boolean", "Undefined", "undefined"],
        "id-match": "error",
        // "import/no-default-export": "error",
        // "import/no-deprecated": "warn",
        // "import/no-extraneous-dependencies": [
        //     "error",
        //     {
        //         devDependencies: false
        //     },
        //     {
        //         optionalDependencies: false
        //     }
        // ],
        "import/no-internal-modules": "off",
        "import/no-unassigned-import": "off",
        "import/order": "off",
        // "indent": "error",
        "linebreak-style": ["off", "windows"],
        "max-classes-per-file": "off",
        "max-len": [
            "error",
            {
                code: 140
            }
        ],
        "max-lines": "off",
        "new-parens": "error",
        "newline-per-chained-call": "off",
        "no-bitwise": "error",
        "no-caller": "error",
        "no-cond-assign": "error",
        "no-console": [
            "error",
            {
                // allow: [
                //     "log",
                //     "warn",
                //     "dir",
                //     "timeLog",
                //     "assert",
                //     "clear",
                //     "count",
                //     "countReset",
                //     "group",
                //     "groupEnd",
                //     "table",
                //     "dirxml",
                //     "error",
                //     "groupCollapsed",
                //     "Console",
                //     "profile",
                //     "profileEnd",
                //     "timeStamp",
                //     "context"
                // ]
            }
        ],
        "no-debugger": "error",
        "no-duplicate-case": "error",
        "no-duplicate-imports": "error",
        "no-empty": "off",
        "no-empty-function": "off",
        "no-eval": "error",
        "no-fallthrough": "off",
        "no-invalid-this": "error",
        "no-irregular-whitespace": "error",
        "no-magic-numbers": "off",
        "no-multiple-empty-lines": [
            "error",
            {
                max: 2
            }
        ],
        "no-new-wrappers": "error",
        "no-null/no-null": "off",
        "no-param-reassign": "off",
        "no-redeclare": "error",
        "no-restricted-imports": "off",
        "no-return-await": "error",
        "no-sequences": "error",
        "no-shadow": "off",
        "no-sparse-arrays": "error",
        "no-template-curly-in-string": "error",
        "no-throw-literal": "error",
        "no-trailing-spaces": "error",
        "no-undef-init": "error",
        "no-underscore-dangle": [
            "error",
            {
                allowAfterThis: true
            }
        ],
        "no-unsafe-finally": "error",
        "no-unused-expressions": "error",
        "no-unused-labels": "error",
        "no-use-before-define": "off",
        "no-var": "error",
        "no-void": "off",
        "object-shorthand": ["error", "never"],
        "one-var": ["error", "never"],
        "padding-line-between-statements": [
            "off",
            {
                blankLine: "always",
                prev: "*",
                next: "return"
            }
        ],
        // "prefer-arrow/prefer-arrow-functions": "error",
        "prefer-const": "error",
        "prefer-object-spread": "error",
        "prefer-template": "off",
        "quote-props": ["error", "consistent"],
        "quotes": "off",
        "radix": "error",
        "semi": "error",
        "space-before-function-paren": "off",
        "space-in-parens": ["error", "never"],
        "spaced-comment": [
            "error",
            "always",
            {
                markers: ["/"]
            }
        ],
        "unicorn/filename-case": "off",
        "unicorn/prefer-switch": "off",
        "unicorn/prefer-ternary": "off",
        "use-isnan": "error",
        "valid-typeof": "off",
        "yoda": "error",
        "@typescript-eslint/tslint/config": [
            "error",
            {
                rules: {
                    "encoding": true,
                    // "import-destructuring-spacing": true,
                    "import-spacing": true,
                    "match-default-export-name": true,
                    "no-mergeable-namespace": true,
                    "no-unnecessary-callback-wrapper": true,
                    // "no-unused-css": true,
                    "number-literal-format": true,
                    "prefer-method-signature": true,
                    "prefer-while": true,
                    "return-undefined": true,
                    "strict-type-predicates": true,
                    "switch-final-break": true,
                    "typedef": [
                        true,
                        "call-signature",
                        "arrow-call-signature",
                        "parameter",
                        "arrow-parameter",
                        "property-declaration",
                        "variable-declaration",
                        "member-variable-declaration",
                        "object-destructuring",
                        "array-destructuring"
                    ],
                    "whitespace": [
                        true,
                        "check-branch",
                        "check-decl",
                        "check-operator",
                        "check-module",
                        "check-separator",
                        "check-rest-spread",
                        "check-type",
                        "check-typecast",
                        "check-type-operator",
                        "check-preblock"
                    ]
                }
            }
        ]
    }
};
