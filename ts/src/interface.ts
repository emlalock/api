import { ExternalLinkType, WheelOfFortuneOperator } from "./enums";

export interface Displaymode {
    timepassed: number;
    timeleft: number;
    showapproximate: number;
    surpriseme: number;
}
export interface ErrorObject {
    error: any;
}
export interface ExternalLink {
    chastitysessionid: string;
    duration: number;
    externallinkid: string;
    linktype: ExternalLinkType;
    usages: number;
    userid: string;
    validuntil: number;
}
export interface ExternalLinkRequest {
    externallinkid: string;
}
export interface Games {
    bacljack: number;
    slotmashine: number;
    bingo: number;
    voting: number;
    wheeloffortune: number;
}
export interface HolderFromTo {
    userid: string;
    apikey: string;
    holderapikey: string;
    from: number;
    to: number;
    text?: string;
}
export interface HolderHistory {
    holderid: string;
    rating: number;
}
export interface HolderValue {
    userid: string;
    apikey: string;
    holderapikey: string;
    value: number;
    text?: string;
}
export interface Info {
    user: User;
    chastitysession: Session;
    error?: string;
}
export interface InstaLink {
    add: number;
    session: InstaLinkSession;
    usages: number;
    used: number;
    error?: string;
}
export interface InstaLinkSession {
    wearer: InstaLinkWearer;
    sessionid: string;
    sessionstart: number;
}
export interface InstaLinkWearer {
    userid: string;
    username: string;
}
export interface Link {
    error?: string;
    chastitysession: Session;
    externallink: ExternalLink;
    wearer: User;
}
export interface Session {
    error: string;
    chastitysessionid: string | undefined;
    creatorid: string;
    wearerid: string;
    holderid: string;
    inoneoercentrange: number;
    status: number;
    sessiontype: number;
    interval: number;
    keyword: string;
    testsession: number;
    feed: number;
    friendlink: number;
    reqlink: number;
    offer: number;
    minimumholderscore: number;
    holdergender: number;
    transfer: number;
    holderfriendlink: number;
    offeruserid: string;
    requirements: number;
    friendlinkadd: number;
    friendlinksub: number;
    reqlinkadd: number;
    reqlinksub: number;
    durationtype: number;
    randomduration: number;
    startduration: number;
    minduration: number;
    maxduration: number;
    beatby: number;
    specstartduration: number;
    specminduration: number;
    specmaxduration: number;
    startweight: number;
    targetweight: number;
    displaymode: Displaymode;
    withpenaltypenalty: number;
    games: Games;
    voting: number;
    mustgamesperday: number;
    mustgamesperiod: number;
    gamesperday: number;
    gamesperiod: number;
    riskvoting: number;
    description: string;
    wheeloffortune: WheelOfFortuneOptions[];
    hygieneopening: number;
    cleaningsperday: number;
    cleaningperiod: number;
    timeforcleaning: number;
    cleaningaction: number;
    cleaningpenalty: number;
    penaltygamesminimum: number;
    startdate: number;
    enddate?: number | string;
    canbeclosed: number;
    playedgamesmust: number;
    currentweight: number;
    weightbalance: number;
    lastverification: number;
    cleanings: number;
    lastweightupdate: number;
    playedgames: number;
    duration: number;
    pictureby: number;
    timeinlock: number;
    created: number;
    closedate: number;
    incleaning: number;
    cleaningstarted: number;
    applyriskvoting: number;
    timewithpenaltyfrom: number;
    timewithpenaltyto: number;
    endtype: number;
    wheelofsessionend: WheelOfFortuneOptions[];
    pillory: number;
    pilloryleft: number;
    pillorymessage: string;
    pillorypenalty: number;
    wearerrating: number;
    holderrating: number;
    percentageperperiod: number;
    percentageperiod: number;
    chance: number;
    holderhistory: HolderHistory;
    locktober: number;
}
export interface User {
    userid: string;
    username: string;
    lastclick: number;
    subscriptionvalue: number;
    financialinterests: number;
    wearerrating: number;
    wearerrates: number;
    holderrating: number;
    holderrates: number;
    ageverified: number;
    sessions: number;
    failedsessions: number;
    maxsession: number;
    minsession: number;
    sumsession: number;
    membersince: number;
    dateofbirth: number;
    description: string;
    gender: number;
    language: string;
    chastityrole: number;
    timezone: number;
    visibilities: Visibilities;
    discordvisible: number;
}
export interface UserIdentifier {
    userid: string;
}
export interface Visibilities {
    userpagevisible: number;
    sessionpagevisible: number;
    feedpagevisible: number;
    highscore: number;
    profilepictures: number;
    showholdersessiononprofile: number;
}
export interface WearerAuth {
    userid: string;
    apikey: string;
}
export interface WearerFromTo {
    userid: string;
    apikey: string;
    from: number;
    to: number;
    text?: string;
}
export interface WearerValue {
    userid: string;
    apikey: string;
    value: number;
    text?: string;
}
export interface WearerInstaLinkValue {
    userid: string;
    apikey: string;
    value: number;
    usages?: number;
    duration?: number;
}
export interface WheelOfFortuneOptions {
    duration: number;
    operator: WheelOfFortuneOperator;
    color: string;
    text: string;
}
