import axios, { AxiosResponse } from "axios";
import { URLSearchParams } from "url";
import {
    ExternalLinkRequest,
    HolderFromTo,
    HolderValue,
    WearerFromTo,
    WearerValue,
    ErrorObject,
    Info,
    Link,
    WearerAuth,
    UserIdentifier,
    WearerInstaLinkValue,
    ExternalLink,
    InstaLink,
} from "./interface";

export const baseApiUrl: string = "https://api.emlalock.com/";

export async function request(endpoint: string, params: URLSearchParams | undefined = undefined): Promise<object | string> {
    const url: string = baseApiUrl + endpoint + (params !== undefined ? "?" + params.toString() : "");
    try {
        const result: AxiosResponse = await axios.get(url);
        return result.data;
    } catch (error) {
        return {
            error: error,
        } as ErrorObject;
    }
}
export async function add(params: WearerValue): Promise<Info | ErrorObject> {
    return (await request("add", setParams(params))) as Info | ErrorObject;
}
export async function addInstaLink(params: WearerInstaLinkValue): Promise<ExternalLink | ErrorObject> {
    return (await request("addInstaLink", setParams(params))) as ExternalLink | ErrorObject;
}
export async function addMax(params: WearerValue): Promise<Info | ErrorObject> {
    return (await request("addmaximum", setParams(params))) as Info | ErrorObject;
}
export async function addMin(params: WearerValue): Promise<Info | ErrorObject> {
    return (await request("addminimum", setParams(params))) as Info | ErrorObject;
}
export async function getProfilepicture(userid: string): Promise<string | ErrorObject> {
    return (await request("profilepicture", setParams({ userid: userid }))) as string | ErrorObject;
}
export async function addRandom(params: WearerFromTo): Promise<Info | ErrorObject> {
    return (await request("addrandom", setParams(params))) as Info | ErrorObject;
}
export async function addRequirement(params: WearerValue): Promise<Info | ErrorObject> {
    return (await request("addrequirement", setParams(params))) as Info | ErrorObject;
}
export async function checkInstaLink(params: ExternalLinkRequest): Promise<InstaLink | ErrorObject> {
    return (await request("checkInstaLink", setParams(params))) as InstaLink | ErrorObject;
}
export async function getExternalLink(params: ExternalLinkRequest): Promise<Link | ErrorObject> {
    return (await request("externallink", setParams(params))) as Link | ErrorObject;
}
export async function getInfo(params: WearerAuth): Promise<Info | ErrorObject> {
    return (await request("info", setParams(params))) as Info | ErrorObject;
}
export function setParams(
    params: ExternalLinkRequest | HolderValue | HolderFromTo | UserIdentifier | WearerAuth | WearerValue | WearerFromTo,
): URLSearchParams {
    const paramArray: Array<[string, string]> = [];
    for (const [key, value] of Object.entries(params)) {
        paramArray.push([key.toString(), value.toString()]);
    }
    return new URLSearchParams(paramArray);
}
export async function sub(params: HolderValue): Promise<Info | ErrorObject> {
    return (await request("sub", setParams(params))) as Info | ErrorObject;
}
export async function subMax(params: HolderValue): Promise<Info | ErrorObject> {
    return (await request("submaximum", setParams(params))) as Info | ErrorObject;
}
export async function subMin(params: HolderValue): Promise<Info | ErrorObject> {
    return (await request("subminimum", setParams(params))) as Info | ErrorObject;
}
export async function subRandom(params: HolderFromTo): Promise<Info | ErrorObject> {
    return (await request("subrandom", setParams(params))) as Info | ErrorObject;
}
export async function subRequirement(params: HolderValue): Promise<Info | ErrorObject> {
    return (await request("subrequirement", setParams(params))) as Info | ErrorObject;
}
